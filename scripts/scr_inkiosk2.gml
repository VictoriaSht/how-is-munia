
myDialogue[0,0] = '- Добрый день. Что вам?';
myDialogue[0,1] = '"Хороший музон."' + '019';
myDialogue[0,2] = 'Продать зажигалку.' + '016';
myDialogue[0,3] = 'Купить ручку.' + '001';
myDialogue[0,4] = 'Уйти.' + '020';
myDialogue[1,0] = '- Ты наверно думаешь: "хоть бы этот продавец не#подумал, что я настолько рассеянный, что забыл взять#ручку, прекрасно зная, что придется заполнять анкету."';
myDialogue[2,0] = '- И что ты думаешь? Я так думаю.';
myDialogue[3,0] = '- Но не беспокойся, тут таких бывает много.';
myDialogue[4,0] = '- Но только не во время перерыва.';
myDialogue[5,0] = '- Да, парень, ты странный. Можно же было бы пойти#куда-нибудь в другое место за ручкой, пока перерыв.#Здесь цены завышены до небес.';
myDialogue[6,0] = 'У меня нет денег.'; mySprite[6] = t1;
myDialogue[7,0] = '- ...    #Как ты тогда собираешься купить ручку?#Я же правильно понял: тебе нужна ручка?'; mySprite[7] = -1;
myDialogue[8,0] = 'Хочу ее опробовать.'; mySprite[8] = t1;
myDialogue[9,0] = '- А-ха, ты собираешься вот так нагло украсть у меня#ручку? Или ты собираешься заполнить анкету и вернуть#ее?'; mySprite[9] = -1;
myDialogue[10,0] = '...'; mySprite[10] = t1;
myDialogue[11,0] = '- Смысл этого киоска в том, что мы такого не позволяем#и заставляем тратить деньги.'; mySprite[11] = -1;
myDialogue[12,0] = '- А-ха-ха, но вообще почему бы и не позволить для#разнообразия, иногда чудеса должны происходить.';
myDialogue[13,0] = '(Продавец протянул ручку через дыру в стекле. Болтун#заполнил анкету, не придумав ничего оригинальнее#своих собственных данных.)';
myDialogue[14,0] = '(Легенда в том, что десять часов назад умер человек с#его именем и адресом, но другой датой рождения.)';
myDialogue[15,0] = '(Болтун вернул ручку, кивнул в знак благодарности и#отошел от киоска.)'; gotoD[15] = 21;
myDialogue[16,0] = '- Хаха, зачем мне эта штука? Если это#завуалированный способ пригласить меня "позажигать#где-нибудь", я не могу. Я на работе.';
myDialogue[17,0] = '- ...Ты наверно подумал, что мне очень скучно тут#сидеть, и все мои мысли только о вечеринках и#развлечениях.';
myDialogue[18,0] = '- Ты прав.';
myDialogue[18,1] = '"Хороший музон."' + '019';
myDialogue[18,2] = 'Купить ручку.' + '001';
myDialogue[18,3] = 'Уйти.' + '020';
myDialogue[19,0] = '- Ха-ха, спасиб. Никогда не устану это говорить.';
myDialogue[19,1] = '"Хороший музон."' + '019';
myDialogue[19,2] = 'Продать зажигалку.' + '016';
myDialogue[19,3] = 'Купить ручку.' + '001';
myDialogue[19,4] = 'Уйти.' + '020';
myDialogue[20,0] = '- Ну пока.';
myDialogue[21,0] = 0; mySprite[21] = 0; gotoD[21] = 21;
