///Create choices for player
for(i = 1; i < array_length_2d(messageGiver.myDialogue, index1); ++i) {
    ++index2;
    choiceBox[i - 1] = instance_create(view_xview[view_current] + 100, view_yview[view_current] + (i * 100), obj_choice_box);
   with(choiceBox[i - 1]) {
        maxLength = sprite_width - 10;
        maxHeight = sprite_height - 10;
        myChoice = messageGiver.myDialogue[index1, index2];
        //Parse string
        myNumber = string_copy(myChoice, string_length(myChoice) - 2, 3);
        myNumber = real(myNumber);
        myChoice = string_copy(myChoice, 1, string_length(myChoice) - 3);
        image_speed = 0;
    }
}
showingChoices = true;
objDialogSystem.alarm[0] = 10;
