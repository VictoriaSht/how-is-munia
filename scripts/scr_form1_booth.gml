myDialogue[0,0] = '...'; mySprite[0] = b1;
myDialogue[1,0] = '*издалека*#     Да?.. Иду-иду.'; mySprite[1] = b1;
myDialogue[2,0] = 'Здравствуйте.'; mySprite[2] = b1;
myDialogue[3,0] = 'Теперь на пост подошел человек, который тоже был в#костюме, но у него был бейджик сотрудника.#Болтун максимально лаконично спросил про отца#Файсаля.'; mySprite[3] = -1;
myDialogue[4,0] = 'Мм? Д-да, странная история...'; mySprite[4] = b1;
myDialogue[5,0] = 'А что?'; mySprite[5] = b1;
myDialogue[6,0] = 'Я ищу его сына.'; mySprite[6] = t1;
myDialogue[7,0] = 'О, вы знаете его сына? Фисаль, кажется?'; mySprite[7] = b1;
myDialogue[8,0] = '...         #...                         #Да.'; mySprite[8] = t1;
myDialogue[9,0] = 'Он заполнил форму по телефону, а потом#исчез.'; mySprite[9] = b1;
myDialogue[10,0] = 'Старика пронесли по аллее и кремировали#в назначенное время, но он так и не#пришел.'; mySprite[10] = b1;
myDialogue[11,0] = '...'; mySprite[11] = t1;
myDialogue[12,0] = 'Вы же все равно его ищете, можете#передать ему поминальный прах?'; mySprite[12] = b1;
myDialogue[13,0] = '...Точнее, половину. Его мать забрала#другую, сказала - вряд ли в ближайшее#время с ним увидится.'; mySprite[13] = b1;
myDialogue[14,0] = '(Болтун кивнул и взял протянутую ему металлическую#коробочку.)'; mySprite[14] = -1;
myDialogue[15,0] = 'Могу еще чем-нибудь помочь?'; mySprite[15] = b1;
myDialogue[16,0] = '(Болтун хотел кивнуть еще раз, но понял, что теперь#надо ответить "нет".)'; mySprite[16] = -1;
myDialogue[17,0] = '(Он помотал головой, кивнул в качестве прощания и#отошел от будки.)'; mySprite[17] = -1;
myDialogue[18,0] = '(Взгляд в сторону входа - оказывается, ребята уже#вернулись.)'; mySprite[17] = -1;
myDialogue[19,0] = 0; mySprite[19] = 0; gotoD[19] = 20;

