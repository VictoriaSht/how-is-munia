///Begin dialog between player and messagegiver

    myMessage = messageGiver.myDialogue[index1, index2];
    DSprite = messageGiver.mySprite[index1];
if !(instance_exists(obj_dialogue_box))
dialoguebox = instance_create(view_xview[view_current] + 960,  view_yview[view_current] + 865, obj_dialogue_box);
else dialoguebox = obj_dialogue_box;
with(dialoguebox) {
    draw_set_font(fnt_for_dialogue);
    
    if((array_length_2d(messageGiver.myDialogue, index1)) > 1 && messageGiver.myDialogue[index1, 1] != 0 )
        hasChoices = true;
    else
        hasChoices = false;
        
    currentText = "";
    position = 0;
}
